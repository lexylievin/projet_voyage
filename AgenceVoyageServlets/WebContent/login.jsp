<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
    <h1>Connexion</h1>
	<form method="post" action="controleur?cde=login">
       <p>
           <label for="prenom">Prenom : </label>
           <input type="text" name="prenom" id="prenom" />
       </p>
       <p>
           <label for="password">Mot de passe : </label>
           <input type="password" name="password" id="password" />
       </p>
       
       <input type="submit" />
   </form>
   <p>Vous n'avez pas de compte cree en un <a href="controleur?cde=co">inscription</a></p>
   <c:if test="${ !empty utilisateur.prenom }"><p><c:out value="Bonjour, ${ nom } ravie de vous revoir" /></p></c:if>
</body>
</html>