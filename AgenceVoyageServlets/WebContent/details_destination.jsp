<%@ page language="java" import="java.util.*,org.bovoyage.servlet.*,org.bovoyage.metier.*" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>${initParam.titre_site}</title>
    <meta http-equiv="Content-Type" Content="text/html; charset=Windows-1252">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="bovoyage.css">
</head>
  
  <body>
  <table align="center">
  	<tr><td><%@ include file="haut_page.jsp" %></td></tr>
  	<tr><td id="MAIN">
    	<h2>Votre futur s�jour : ${destination.pays }</h2>
		<table width='500px'>
		<tr><td>${destination.description }</td></tr>
		<tr><td>
			<table><tr>
			<c:forEach items="${destination.images }" var="img" >
			<td><img src="images/${img }" /></td>
			</c:forEach>
			 </tr></table>
			 <table border='1'>
			 	<tr><th>D�part</th><th>Retour</th><th>Prix TTC</th><th></th><th></tr>
			 	
			 	<c:forEach items="${ destination.dates}" var="sejour" varStatus="st">
			 		<c:if test="${st.count % 2 == 0 }">
			 			<c:set var="couleur" value="LightSteelBlue"/>
			 		</c:if>
			 		<c:if test="${st.count % 2 != 0 }">
			 			<c:set var="couleur" value="white"/>
			 		</c:if>
			 		<fmt:setLocale value="fr" />
			 		<tr  bgcolor="${couleur }">
			 			<td><fmt:formatDate value="${sejour.depart }"  dateStyle="full" /></td>
			 			<td><fmt:formatDate value="${sejour.retour }"  dateStyle="full" /></td>
			 			<td><fmt:formatNumber value="${sejour.prix }"  type="currency" currencySymbol="&euro;" /></td>
			 			<td><a href="controleur?cde=cde&id=${sejour.id }">Commander</td></tr>
			 	</c:forEach>

			 </table>
		</td></tr>
		</table>
    </td></tr>
    <tr><td><a href='controleur?cde=<%=Constantes.CDE_ACCUEIL %> '>Retour � l'accueil</a></td></tr>
    <tr><td><%@ include file="bas_page.jsp" %></td></tr>
  </table>
  </body>
  </html>
