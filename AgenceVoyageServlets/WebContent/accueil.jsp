<%-- <%@ page language="java" import="org.bovoyage.servlet.*,org.bovoyage.metier.*" pageEncoding="ISO-8859-15"%>
--%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>${initParam.titre_site}</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="bovoyage.css">
</head>
  
  <body>
  <table align="center">
  	<tr><td><%@ include file="haut_page.jsp" %></td></tr>
  	<tr><td id="MAIN">
    	<h2>Bienvenue sur notre site de voyage</h2>
    	<h3>Liste des destinations</h3>
    	<table>
    	<c:forEach var="destination" items="${destinations}">
    		<tr><td>${destination.pays }</td><td><a href="controleur?cde=det&id=${destination.identifiant }">d�tails</a></td></tr>
    	</c:forEach>
    	</table>
    </td></tr>
    <tr><td><%@ include file="bas_page.jsp" %></td></tr>
  </table>
  </body>
  </html>
