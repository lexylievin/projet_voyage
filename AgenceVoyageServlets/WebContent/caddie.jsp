<%@ page language="java" import="java.util.*,org.bovoyage.servlet.*,org.bovoyage.metier.*" pageEncoding="ISO-8859-15"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>${initParam.titre_site}</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<link rel="stylesheet" type="text/css" href="bovoyage.css">
</head>
  <fmt:setLocale value="fr" />
  <body>
  <table align="center">
  	<tr><td><%@ include file="haut_page.jsp" %></td></tr>
  	<tr><td id="MAIN">
    	<h2>Votre commande</h2>
    	<form action="controleur?cde=chgt" method="post">
    	<table border='1'>
		<tr><th>Destination</th><th>D�part</th><th>Retour</th><th>Nb personnes</th><th>Prix Total TTC</th></tr>
		<tr><td>${caddie.pays }</td>
				<td><fmt:formatDate value="${caddie.sejour.depart }" dateStyle="full" /></td>
				<td><fmt:formatDate value="${caddie.sejour.retour }" dateStyle="full"  /></td>
				<td align="center">
					<select name="nb_personnes" onChange="document.forms[0].submit();">
						<c:forEach var="i" begin="1" end="10">
							<c:if test="${i == caddie.nbPersonnes }" >
								<c:set var="selected" value="selected='selected'"/>
							</c:if>
							<c:if test="${i != caddie.nbPersonnes }" >
								<c:set var="selected" value=""/>
							</c:if>
							<option value="${i }" ${selected }>${i }</option>
						</c:forEach>
					</select>
				</td>
				<td><fmt:formatNumber value="${caddie.prixTotal }" type="currency" currencySymbol="&euro;" /></td></tr>

    	</table>
    	</form>
    </td></tr>
    <tr><td><a href='controleur?cde=<%=Constantes.CDE_ACCUEIL %> '>Retour � l'accueil</a></td></tr>
    <tr><td><%@ include file="bas_page.jsp" %></td></tr>
  </table>
  </body>
  </html>
