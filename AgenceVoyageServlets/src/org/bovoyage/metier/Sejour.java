package org.bovoyage.metier;

import java.io.Serializable;
import java.util.Date;

public class Sejour implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8032475442746653441L;
	
	private int id;
	private Date depart;
	private Date retour;
	private double prix;
	
	public Sejour()
	{}
	
	public Sejour(Date depart, Date retour, double prix)
	{
		this.depart = depart;
		this.retour = retour;
		this.prix = prix;
	}

	public Date getDepart()
	{
		return depart;
	}

	public void setDepart(Date depart)
	{
		this.depart = depart;
	}

	public Date getRetour()
	{
		return retour;
	}

	public void setRetour(Date retour)
	{
		this.retour = retour;
	}
	
	public double getPrix()
	{
		return prix;
	}

	public void setPrix(double prix)
	{
		this.prix = prix;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String toString()
	{
		return "("+id+") DEP : "+depart+" - RET : "+retour+" prix : "+this.prix;
	}
	
	public boolean equals(Sejour s)
	{
		return this.id == s.id;
	}
	
	public boolean equals(int id)
	{
		return this.id==id;
	}
}
