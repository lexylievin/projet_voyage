package org.bovoyage.metier;

import java.util.*;
import java.io.*;


public class Destination implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9047780353115578697L;

	private int identifiant;

	private String pays;

	private String description;

	private Vector<String> images = null;

	private Vector<Sejour> sejours = null;

	public Destination()
	{
	}

	public Destination(int identifiant, String destination)
	{
		this.identifiant = identifiant;
		this.pays = destination;
	}

	public Vector<Sejour> getDates()
	{
		return sejours;
	}

	public void setDates(Vector<Sejour> dates)
	{
		this.sejours = dates;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getPays()
	{
		return pays;
	}

	public void setPays(String destination)
	{
		this.pays = destination;
	}

	public int getIdentifiant()
	{
		return identifiant;
	}

	public void setIdentifiant(int identifiant)
	{
		this.identifiant = identifiant;
	}

	public Vector<String> getImages()
	{
		return images;
	}

	public void setImages(Vector<String> images)
	{
		this.images = images;
	}

	public void addImage(String image)
	{
		images.add(image);
	}

	public void addSejour(Sejour sejour)
	{
		this.sejours.add(sejour);
	}

	public Sejour getSejour(int id)
	{
		Sejour sejour = null;
		Iterator<Sejour> it = this.sejours.iterator();
		while (it.hasNext())
		{
			sejour = it.next();
			if (sejour.getId() == id)
				break;
		}
		return sejour;
	}

	public Sejour getSejour(String id)
	{
		return this.getSejour(Integer.parseInt(id));
	}

	public String toString()
	{
		String retour = "\n\n";
		retour += pays + "\n" + description;
		Iterator<Sejour> it = sejours.iterator();
		while (it.hasNext())
		{
			retour += "\n\t" + it.next();
		}
		return retour;
	}
}
