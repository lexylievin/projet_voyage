package org.bovoyage.servlet;

public class Constantes
{
	public static String CDE_ACCUEIL	= "acc";
	public static String CDE_CONNEXION  =  "co";
	public static String CDE_LOGIN = "login";
	public static String CDE_DETAILS	= "det";
	public static String CDE_COMMANDE	= "cde";
	public static String CDE_PANIER		= "caddie";
	public static String CDE_CHANGER_NB_PERSONNES = "chgt";
	
	public static String ATTR_REQ_DESTINATIONS = "destinations";
	public static String ATTR_SESSION_DESTINATION = "destination";
	public static String ATTR_SESSION_CADDIE = "caddie";
}
