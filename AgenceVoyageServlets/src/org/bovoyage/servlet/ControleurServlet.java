package org.bovoyage.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bovoyage.bdd.Noms;
import org.bovoyage.dao.DAO;
import org.bovoyage.dao.DestinationDAO;
import org.bovoyage.metier.Destination;
import org.bovoyage.metier.Sejour;
import org.bovoyage.metier.Utilisateur;
import org.bovoyage.metier.Voyage;

import org.bovoyage.dao.DaoFactory;
import org.bovoyage.dao.UtilisateurDao;





public class ControleurServlet extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8437305835510027544L;
	private UtilisateurDao utilisateurDao;
	
	private DAO dao;
	private ServletContext application;
	private Log log = LogFactory.getLog(ControleurServlet.class);
	
	
	public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.utilisateurDao = daoFactory.getUtilisateurDao();
    }

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String page = "/accueil.jsp";
		log.debug("debug entree dans doGet");
		log.info("info entree dans doget");
		application = getServletContext();
		String cde = request.getParameter("cde");
		dao = DAO.getInstance();
		if (cde == null)
			cde = Constantes.CDE_ACCUEIL;
		if (cde.equals(""))
			cde = Constantes.CDE_ACCUEIL;
		
		if (cde.equalsIgnoreCase(Constantes.CDE_ACCUEIL))
			page = this.actionAccueil(request, response);
		if (cde.equalsIgnoreCase(Constantes.CDE_CONNEXION))
			page = this.actionConnexion(request, response);
		if (cde.equalsIgnoreCase(Constantes.CDE_LOGIN))
			page = this.actionLogin(request, response);
		if (cde.equalsIgnoreCase(Constantes.CDE_DETAILS))
			page = this.actionDetails(request, response);
		if (cde.equalsIgnoreCase(Constantes.CDE_COMMANDE))
			page = this.actionCommande(request, response);
		if (cde.equalsIgnoreCase(Constantes.CDE_PANIER))
			page = this.actionAfficherVoyage(request, response);
		if (cde.equalsIgnoreCase(Constantes.CDE_CHANGER_NB_PERSONNES))
			page = this.actionChangerVoyage(request, response);
		
		request.setAttribute("utilisateurs", utilisateurDao.lister());

		RequestDispatcher rd = application.getRequestDispatcher(page);
		rd.forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		System.out.println("je passe dans le dopost");
		
		Utilisateur utilisateur = new Utilisateur();
        utilisateur.setNom(request.getParameter("nom"));
        utilisateur.setPrenom(request.getParameter("prenom"));
        utilisateur.setTelephone(request.getParameter("telephone"));
        utilisateur.setMail(request.getParameter("mail"));
        utilisateur.setPassword(request.getParameter("password"));
        
        utilisateurDao.ajouter(utilisateur);        
        request.setAttribute("utilisateurs", utilisateurDao.lister());

		doGet(request, response);
	}

	private String actionAccueil(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		DestinationDAO dest = new DestinationDAO(dao);
		Vector<Destination> destinations = null;
		try
		{
			destinations = dest.getDestinations();
			log.debug("getDestinations OK");
		} catch (SQLException e)
		{
			log.debug("SQLException");
			throw new ServletException(">>> IMPOSSIBLE DE CHARGER LES DESTINATIONS");
		}
		catch (Exception e) {
			System.out.println("Exception");
		}
		request.setAttribute(Constantes.ATTR_REQ_DESTINATIONS, destinations);
		return "/accueil.jsp";
	}
	
	private String actionConnexion(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException
	{
		return "/connexion.jsp";
	}
	
	private String actionLogin(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException
	{
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setPrenom(request.getParameter("prenom"));
		utilisateur.setPassword(request.getParameter("password"));		
		utilisateurDao.connexion(utilisateur);
		return "/login.jsp";
	}

	private String actionDetails(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String idDestination = request.getParameter("id");
		DestinationDAO dest = new DestinationDAO(dao);
		Destination destination = null;
		try
		{
			destination = dest.getDestination(idDestination);
		} catch (SQLException e)
		{
			throw new ServletException(">>> IMPOSSIBLE DE CHARGER LA DESTINATION "+idDestination);
		}
		request.getSession().setAttribute(Constantes.ATTR_SESSION_DESTINATION, destination);
		return "/details_destination.jsp";
	}

	private String actionCommande(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String idSejour = request.getParameter("id");
		Destination destination = (Destination)request.getSession().getAttribute(Constantes.ATTR_SESSION_DESTINATION);
		Sejour sejour = destination.getSejour(idSejour);
		Voyage voyage = new Voyage(sejour,destination.getPays(),1);
		request.getSession().setAttribute(Constantes.ATTR_SESSION_CADDIE, voyage);
		return "/details_destination.jsp";
	}

	private String actionAfficherVoyage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		return "/caddie.jsp";
	}
	
	private String actionChangerVoyage(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException
	{
		Voyage v = (Voyage)request.getSession().getAttribute(Constantes.ATTR_SESSION_CADDIE);
		v.setNbPersonnes(request.getParameter("nb_personnes"));
		return this.actionAfficherVoyage(request, response);
	}


}
