package org.bovoyage.dao;

import java.util.List;

import org.bovoyage.metier.Utilisateur;

public interface UtilisateurDao {
    void ajouter( Utilisateur utilisateur );
    String connexion ( Utilisateur utilisateur );
    List<Utilisateur> lister();
}
