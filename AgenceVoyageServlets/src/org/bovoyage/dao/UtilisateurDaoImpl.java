package org.bovoyage.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.bovoyage.metier.Utilisateur;

public class UtilisateurDaoImpl implements UtilisateurDao {
    private DaoFactory daoFactory;

    UtilisateurDaoImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public void ajouter(Utilisateur utilisateur) {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO users(id_user,nom, prenom,telephone,mail,password) VALUES(?,?, ?, ?, ?, ?);");
            preparedStatement.setInt(1, (int)(Math.random() + 2) );
            preparedStatement.setString(2, utilisateur.getNom());
            preparedStatement.setString(3, utilisateur.getPrenom());
            preparedStatement.setString(4, utilisateur.getTelephone());
            preparedStatement.setString(5, utilisateur.getMail());
            preparedStatement.setString(6, utilisateur.getPassword());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public String connexion(Utilisateur utilisateur) {
    	
    	Connection connexion = null;
        PreparedStatement preparedStatement = null;
        boolean resultat = false;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT ?, ? FROM users);");
            preparedStatement.setString(1, utilisateur.getPrenom());
            preparedStatement.setString(2, utilisateur.getPassword());
            resultat = preparedStatement.execute();
            if(resultat != false) {
            	return utilisateur.getPrenom();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return utilisateur.getPrenom();
    	
    }

    @Override
    public List<Utilisateur> lister() {
        List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT nom, prenom FROM users;");

            while (resultat.next()) {
                String nom = resultat.getString("nom");
                String prenom = resultat.getString("prenom");

                Utilisateur utilisateur = new Utilisateur();
                utilisateur.setNom(nom);
                utilisateur.setPrenom(prenom);

                utilisateurs.add(utilisateur);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return utilisateurs;
    }

}
